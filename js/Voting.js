Vue.component('voting', {
  props: {
    submission: {
      type: Object,
      required: true,
    },
    isVoted: {
      type: Boolean,
      required: true,
    },
  },
  methods: {
    vote() {
      submitVote(this.submission.handle);

      this.$emit('voted');
    },
  },
  computed: {
    photoPath() {
      return `https://fatihacet.gitlab.io/gitlab-theme-call/images/${this.submission.photo}`;
    },
  },
  template: `
    <div class="tile tile-centered">
      <div class="tile-icon">
        <img :src="photoPath" width="120" />
      </div>
      <div class="tile-content">
        <div class="tile-title">
          <mark>@{{submission.handle}}</mark>
        </div>
      </div>
      <div v-if="!isVoted" class="tile-action">
        <button @click="vote" class="btn btn-link disabled">Vote</button>
      </div>
    </div>
  `
});

new Vue({
  el: '#voting',
  data: {
    isLoading: false,
    isVoted: false,
    submissions: [],
    votes: {},
    totalVotes: 0,
  },
  methods: {
    handleVote() {
      // this.isVoted = true;

      getVotes(({ votes, totalVotes }) => {
        this.votes = votes;
        this.totalVotes = totalVotes;
      });
    },
  },
  created() {
    this.isLoading = true;

    fetchDataFromFirebase((data) => {
      this.submissions = data;
      this.isLoading = false;

      this.handleVote();
    });
  },
});
