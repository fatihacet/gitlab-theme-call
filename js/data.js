const exportedData = {
  "ClemMakesApps" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "ClemMakesApps",
        "photo" : "ClemMakesApps-IMG_8837-2.JPG",
        "story" : "Propeller plane flying over Poland"
      }
    }
  },
  "afontaine" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "afontaine",
        "photo" : "afontaine-MVIMG_20180215_174450-EFFECTS.jpg",
        "story" : "It was just a really foggy day in London, ON, and the atmosphere was so moody. I think it was captured pretty well. Google auto-magicked it a bit, which is nice of them to do.\n\n90% sure I was on my way to grab a burrito at the time, everything else was luck."
      }
    }
  },
  "andr3" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "andr3",
        "photo" : "andr3-46396420895_3677eba01a_o.jpg",
        "story" : "The Bucket Shot. After being with GitLab for just a month, I travelled to Iceland to capture this. This is under the belly of a glacier in Vatnajökull National Park in the South Coast of Iceland. It involved 6hr drive on the day before, amongst glacier rivers, waterfalls, moss lava fields and greeted at night with a perfect weather under a crazy aurora sky... we slept under a mountain next to this glacier so we could beat the tourists in the morning. We hiked for over an hour with crampons, me and a friend, before we arrived in this beauty.. just behind Jökulsárlón, the famed glacier lagoon.\n\nMore pics of that trip here: https://www.instagram.com/explore/tags/andr3returnstoiceland2018/"
      }
    }
  },
  "annabeldunstone" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "annabeldunstone",
        "photo" : "annabeldunstone-grammy1.jpg",
        "story" : "This is my sweet Grammy around 2009. She was diagnosed with Alzheimer's a year or so prior, and at this point she would zone out frequently and stare out the window. Since I was majoring in photography I always had my camera with me, and took several photos of her like this. \n\nIt's shot on black & white 35mm film on my Canon AE-1, which is no longer functioning :("
      }
    }
  },
  "dennis" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "dennis",
        "photo" : "dennis-IMG_20181201_172802.jpg",
        "story" : "In Malaga, Spain, with my girlfriend and we were checking out the different beaches and taking in the views. Random (hopefully neighborhood) dog comes up to us and is following us around for the time we were at this beach. Combine the timing of the impending sunset and us giving the dog some love, and you get this photo!"
      }
    }
  },
  "ealcantara" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "ealcantara",
        "photo" : "ealcantara-hermit-crab%20in%20the%20window.jpg",
        "story" : "My friends and I decided to visit Montecristi, a province in the northwest part of the Dominican Republic that borderlines with Haiti and has some of the most beautiful mangroves ecosystems on the island. We were staying on this very old and small hotel so close to the beach that we were afraid of the high tides at night. One morning we decided to explore more of the beach and we found several abandoned houses with peculiar designs where the sea had recovered some ground. Their floors were full of sand, dead coral, and of course, some small sea creatures like this hermit crab I found walking in the window of one of the houses.\n\nI decided to be extremely patient (in spite of exasperating my friends who wanted to move on) and wait until the little friend would stay in a nice position so I could get the shot I wanted. "
      }
    }
  },
  "ekigbo" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "ekigbo",
        "photo" : "ekigbo-1788-0009.JPG",
        "story" : "This was our first trip to Japan in 2014, I don't really remember taking this photo but it was somewhere in Osaka, probably shot on my Voigtlander Bessa R. I mostly shoot film, but for some reason had forgotten to send this roll to be processed, we found the roll in the back of the fridge around 2017 and this photo has cracked me up ever since. He just looks really quirky and cool and kinda confident."
      }
    }
  },
  "fatihacet" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "fatihacet",
        "photo" : "fatihacet-DSC_2497.JPG",
        "story" : "This picture was taken on April 2017, only 3 months before our son has born aka the last couple trip before beginning a new chapter. I took this picture at Seven Lakes National Park, Bolu, Turkey with my Nikon D3200 camera."
      }
    }
  },
  "jivanvl" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "jivanvl",
        "photo" : "jivanvl-moments_ab4013a4-9d69-4a34-83fb-a415138ac10b.jpg",
        "story" : "This is just a random picture took by a stranger, it's my friends and myself having a stroll at night, thanks random stranger"
      }
    }
  },
  "kushalpandya" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "kushalpandya",
        "photo" : "kushalpandya-20370081631_0898e7bbe5_o.jpg",
        "story" : "This picture was taken on my bike trip to Lake Tsomoriri in Ladakh (India), I chose this photo not only because it was scenic and I just stopped my bike on road and took it but I was in a place where there were no human establishments at least in 200 KM (~120 miles) of radius and the road too was barely a road and the entire ride was surreal experience that I would never forget."
      }
    }
  },
  "leipert" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "leipert",
        "photo" : "leipert-20110427_080055%20_MG_1925.jpg",
        "story" : "I was living in Israel for a while. Met this little fella while gardening."
      }
    }
  },
  "markrian" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "markrian",
        "photo" : "markrian-portes%20du%20soleil.jpg",
        "story" : "I try to go skiing with friends in Les Portes du Soleil every year. This is one of the reasons why!"
      }
    }
  },
  "mfluharty" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "mfluharty",
        "photo" : "mfluharty-2019-04-02%2007.56.23.jpg",
        "story" : "I love this one because the focus was just right and it captured the moment perfectly. It was taken on a ferry - we noticed ladybugs all over the port area and this little guy tagged along for the ride. "
      }
    }
  },
  "mishunov" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "mishunov",
        "photo" : "mishunov-pic.png",
        "story" : "Nothing really special: just a foggy morning right outside of the hotel where I had to live for a month while searching for a new apartment. To me it really symbolises a bridge connecting \"old\" and \"new\" life since I've just started the divorce process back then :)"
      }
    }
  },
  "nfriend" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "nfriend",
        "photo" : "nfriend-pei.jpg",
        "story" : "This is a photo I snapped from a plane while flying out of Prince Edward Island, Canada (where I now live).  I was just leaving from a weekend visit to PEI to visit my girlfriend (now wife :) ).  We dated remotely for about 10 months before I quit my job in the States and immigrated to PEI."
      }
    }
  },
  "sarahghp" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "sarahghp",
        "photo" : "sarahghp-IMG_8019.jpg",
        "story" : "It's my art projected on to the band at my friend's show."
      }
    }
  },
  "sbigelow" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "sbigelow",
        "photo" : "sbigelow-07431816-F390-42F4-84A5-C63133D88880.JPG",
        "story" : "Just a pretty shot while hiking in the Redwoods"
      }
    }
  },
  "shampton" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "shampton",
        "photo" : "shampton-IMG_20170526_175113424-EFFECTS.jpg",
        "story" : "Stayed at this bed and breakfast a couple years ago in downtown Salt Lake City, Utah. It's a really pretty building, and the picture turned out great."
      }
    }
  },
  "timzallmann" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "timzallmann",
        "photo" : "timzallmann-MVIMG_20180728_114546%20(2).jpg",
        "story" : "\"Xaver, how tall is the Arc de Triomphe?\" - July 2018"
      }
    }
  },
  "tread" : {
    "submissions" : {
      "best-photo" : {
        "handle" : "tristan.read",
        "photo" : "tread-DSC00712.JPG",
        "story" : "This photo is overlooking The Treasury in the ancient city of Petra, Jordan. You might recognise it from that Indiana Jones movie. The shot was taken at the end of a long climb, after which we rewarded ourselves by perching on this outcrop, sipping Bedouin tea and watching the hubbub below."
      }
    }
  },
  "votes" : {
    "-LbTR0lvCsamT_KV-QQf" : "timzallmann",
    "-LbTR1qwcO400iSveb4h" : "mishunov",
    "-LbTR2029xbRqrwuKL83" : "mishunov",
    "-LbTR2bZkHI8QRytUE9F" : "markrian",
    "-LbTR2fLqSPKWFOkG_7S" : "timzallmann",
    "-LbTR2sjFpfQ058zZiPE" : "annabeldunstone",
    "-LbTR38uQHMIVQ0Y9yoc" : "andr3",
    "-LbTR3VCD7IjETHBFPex" : "andr3",
    "-LbTR3XyBO8Eo_jzG2vX" : "mishunov",
    "-LbTR3YIKISmiwSsiK4M" : "sarahghp",
    "-LbTR3cKS9Jgm5d-QOJn" : "andr3",
    "-LbTR4BUEkW4lLCJ0Rq7" : "andr3",
    "-LbTR4NbOuMC8RDqYOxZ" : "kushalpandya",
    "-LbTR4TrOkBwFTHm3WXj" : "kushalpandya",
    "-LbTR4rdCFiSrdcvdth0" : "kushalpandya",
    "-LbTR5EpKDB8JJY5Qi1K" : "mfluharty",
    "-LbTR5gr--TpyXY4f8oY" : "sbigelow",
    "-LbTR6Cw0G3fZv8-Dx_u" : "sbigelow",
    "-LbTR72O6dVP2yEkrJaX" : "mishunov",
    "-LbTR7A4lpUwKfB0bKns" : "kushalpandya",
    "-LbTR7IIm4VfHYrCs_gH" : "andr3",
    "-LbTR86MyGpwTVAgtqEq" : "afontaine",
    "-LbTRAvLWKn_rbCpGOUS" : "fatihacet",
    "-LbTRFmOXTDMfs_pLc9-" : "andr3",
    "-LbTRI2KIDzLpb43iEog" : "mishunov",
    "-LbTRItMF8ZFJc4YCiGH" : "dennis"
  }
}
