new Vue({
  el: '#app',
  data: {
    isLoading: false,
    submissions: [],
    index: 0,
  },
  methods: {
    next() {
      if (this.index + 1 === this.submissions.length) {
        return;
      }

      this.index = Math.min(this.index + 1, this.submissions.length);
    },
    prev() {
      this.index = Math.max(this.index - 1, 0);
    },
  },
  created() {
    this.isLoading = true;

    fetchDataFromFirebase((data) => {
      this.submissions = data;
      this.isLoading = false;
    });

    // keyup event handlers didn't work. Probably because of HTML template
    window.addEventListener('keyup', (e) => {
      e.keyCode === 37 ? this.prev() : e.keyCode === 39 ? this.next() : '';
    });
  },
});
