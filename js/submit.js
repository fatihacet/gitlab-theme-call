const getFormValues = () => {
  const { elements } = document.forms['submit-form'];

  return {
    handle: elements.handle.value,
    story: elements.story.value,
    photo: elements.photo.files[0],
  };
};

const uploadImage = () => {
  const { handle, photo } = getFormValues();
  const storageRef = firebase.storage().ref();
  const targetRef = storageRef.child(`${handle}-${photo.name}`);

  return targetRef.put(photo);
}

const getFullData = (snapshot) => {
  const baseUrl = 'https://firebasestorage.googleapis.com/v0/b/gitlab-theme-call.appspot.com/o/';
  const imagePath = encodeURI(snapshot.metadata.fullPath);
  const publicUrl = `${baseUrl}${imagePath}?alt=media`;
  const { handle, story } = getFormValues();

  return { handle, story, photo: publicUrl };
}

const save = ({ handle, story, photo }) => {
  const database = firebase.database();

  database.ref(`${handle}/submissions/best-photo`).set({ handle, story, photo });

  return { handle, story, photo };
}

const submitButton = document.querySelector('#submit');
const toggleSubmitButton = () => {
  // Chrome doesn't support multiple classnames for toggle method
  submitButton.classList.toggle('disabled');
  submitButton.classList.toggle('loading');
}

const enablePreview = ({ handle, story, photo }) => {
  const previewEl = document.querySelector('#preview-container');

  previewEl.querySelector('.card-title').innerText = `@${handle}`;
  previewEl.querySelector('.card-body').innerHTML = story;
  previewEl.querySelector('.card-image img').src = photo;
  previewEl.classList.remove('d-none');
}

document.querySelector('#submit').addEventListener('click', () => {
  toggleSubmitButton();

  uploadImage()
    .then(getFullData)
    .then(save)
    .then(enablePreview)
    .then(toggleSubmitButton)
    .catch(toggleSubmitButton);
});
