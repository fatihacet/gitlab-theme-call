Vue.component('submission', {
  props: {
    submission: {
      type: Object,
      required: true,
    },
  },
  data() {
    return {
      hasUser: false,
      user: {},
    };
  },
  computed: {
    photoPath() {
      return `https://fatihacet.gitlab.io/gitlab-theme-call/images/${this.submission.photo}`;
    },
  },
  mounted() {
    fetch(`https://gitlab.com/api/v4/users/?username=${this.submission.handle}`)
      .then(res => res.json())
      .then((res) => {
        this.hasUser = true;
        this.user = res[0];
      });
  },
  template: `
    <div class="card">
      <div class="card-header">
        <div v-if="hasUser">
          <figure class="avatar avatar-lg float-left">
            <img :src="user.avatar_url" />
          </figure>
          <div class="meta float-left ml-2">
            <span>{{ user.name }}</span>
            <span>@{{ user.username }}</span>
          </div>
        </div>
        <div v-else class="card-title h5">@{{ submission.handle }}</div>
      </div>
      <div class="card-body">{{ submission.story }}</div>
      <div class="card-image">
        <img :src="photoPath" class="img-responsive">
      </div>
    </div>
  `
});
