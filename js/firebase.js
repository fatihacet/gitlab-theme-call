var config = {
  apiKey: 'AIzaSyDXalpRaly29dPv5QNWL48s5usOkLYFDBs',
  authDomain: 'gitlab-theme-call.firebaseapp.com',
  databaseURL: 'https://gitlab-theme-call.firebaseio.com',
  projectId: 'gitlab-theme-call',
  storageBucket: 'gitlab-theme-call.appspot.com',
  messagingSenderId: '538616216203'
};

// firebase.initializeApp(config);

const parseData = (data) => {
  return Object.values(data).reduce((acc, item) => {
    if (item.submissions) {
      acc.push(item.submissions['best-photo']);
    }

    return acc;
  }, []);
}

window.fetchDataFromFirebase = (cb) => {
  cb(parseData(exportedData));

  // firebase.database().ref().on('value', (snapshot) => {
  //   const submissions = parseData(snapshot.val());

  //   cb(submissions);
  // });
}

window.submitVote = (handle) => {
  firebase.database().ref('/votes').push(handle);
}

const parseVotes = (data) => {
  const votes = Object.values(data);
  const results = votes.reduce((acc, handle) => {
    if (acc[handle]) {
      acc[handle] = acc[handle] + 1;
    } else {
      acc[handle] = 1;
    }

    return acc;
  }, {});

  const sorted = {};
  Object.keys(results)
    .sort(function(a, b) { return results[b] - results[a] })
    .forEach(key => sorted[key] = results[key]);

  return { votes: sorted, totalVotes: votes.length };
}

window.getVotes = (cb) => {
  cb(parseVotes(exportedData.votes));
  // firebase.database().ref('/votes').on('value', (snapshot) => {
  //   cb(parseVotes(snapshot.val()));
  // });
}
